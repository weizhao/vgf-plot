%% --------------VG AND VF PLOT CODE FOR NASTRAN RESULT--------------------
%  This code is for flutter result *.f06 for NASTRAN
%  CHANGE NUMBER IN THE FOLLOWING CODE USING DIFFERENT FLUTTER METHOD
%
%  "  eval(['vel(:,ijk,:)=data' num2str(ijk) '(:,5,:);'])
%     eval(['damp(:,ijk,:)=data' num2str(ijk) '(:,6,:);'])
%     eval(['freq(:,ijk,:)=data' num2str(ijk) '(:,7,:);']) "
%
%  "5,6,7" is for PKNL method, change it for PK(is 3,4,5),KE method
%     Copy and save it, run it under its directory.
%   --------------------------------------------------------------
%    Developed by Wei Zhao and Vijayakumari H.S. (weizhao@vt.edu)
%   --------------------------------------------------------------
% % First Modified: 25 Feb, 2013
% ----
% % Modified secondly : 8 March,2013--fix the bug to read the accurate
% % divergence speed
% ----
% % Thirdly Modified: 9 March, 2013--fix the bug to read the accurate
% % divergence speed
% ----
% % Fourthly time: April,10,2013---change the V-g, v-f plot into separate plots
% ----
% % 5th time: July, 30,2013---change the column number for vel,damp and
% % freq for PKNL method.
% ----
% % 6TH MODIFIED TIME:August,6,2013----------- Set the critical damping value
% % for interpolating the flutter speed.
% ----
% % 7th modified time: August,7,2013------Convert the velocity into
% % Equivalent Air Speed (EAS) when using PKNL method, and add choice of solution method.
% %
% ----
% % 8th modified time, May 6 2015
% % Add root-locus
% ----- July,5,2017
% % remove zero reduced natural frequency in root locus plot

%% ---------------------------------------------------------------------------------------
clear all;warning off;load lastpath %% comment this line if there is no lastpath

program_folder = pwd;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   LOADING *.f06 FILE TO FIND FLUTTER DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[filename,pathname]=uigetfile([ '*.f06'],'Select Nastran Output File');
fid=fopen([pathname filename]);

results_folder = pathname(1:end-1);
%% Input
% modelname='mAEWing1_Hati_FEMv3p3';
modelname=input('Name for the VGF plot (e.g. mAEWing1_Hati_FEMv3p3): ', 's');
CriticalDampingValue=input('Set the Structural Damping Value: ');
% % method=input('Choose the Number of the Solution Methods(PK-1;PKNL-2;K-Method-3;KE-4): ');
% % while method~=1 && method~=2 && method~=3 && method~=4
% %     display('***Number Input is illegal, please input it once again!*****')
% %     method=input('Choose the Number of the Solution Methods(PK-1;PKNL-2;K-Method-3;KE-4): ');
% % end
method=input('Choose the Number of the Solution Methods(PK-1;K-Method-3;KE-4): ');
while method~=1 && method~=2 && method~=3 && method~=4
    display('***Number Input is illegal, please input it once again!*****')
    method=input('Choose the Number of the Solution Methods(PK-1;K-Method-3;KE-4): ');
end
display('------------------------------------------------------------------')
display('****Attention: Keep in mind the unit of velocity in your input file*******')
display('****************before converting the unit of velocity********************')
display('------------------------------------------------------------------')
ConvertUnit=input('Choose the Velocity Conversion (e.g. present->0;ips->fps: 1; ips->knots:2; user-defined -> 3): ');
while ConvertUnit~=0 && ConvertUnit~=1 &&ConvertUnit~=2 && ConvertUnit~=3
    display('***Number Input is illegal, please input it once again!*****')
    ConvertUnit=input('Choose the Number of Unit of the Velocity will convert (inch/s-0;ft/s-1;knots-2): ');
end
if ConvertUnit ==3
    convert_vel_value=input('Input the velocity conversion: ');
end;
%%
% CriticalDampingValue=[0.02];%Set the Value of Critical Damping Value.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SCANNING FILE FOR UNIQUE STRING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
status=fseek(fid,0,'eof');EOF=ftell(fid);currentFPI=fseek(fid,0,'bof');
hbar=waitbar(0,'Reading file');cc=1;filecounter=0;currentpoint=0;fatalerror=0;subcasestrold='';
currentFPIhistory=zeros(3,1);caseopt=1;
while currentFPI<EOF
    line=fgetl(fid);currentFPI=ftell(fid);waitbar(currentFPI/EOF,hbar);
    str1=findstr(line,'0');
    str2=findstr(line,'FLUTTER');
    str3=findstr(line,'SUMMARY');
    str4=findstr(line,'FATAL');
    if isempty(str4)==0
        fatalerror=1;
%         break
    end
    currentFPIhistory(3)=currentFPIhistory(2);currentFPIhistory(2)=currentFPIhistory(1);
    currentFPIhistory(1)=currentFPI;
    if isempty(str1)==0 && isempty(str2)==0 && isempty(str3)==0
        fseek(fid,currentFPIhistory(3)-currentFPI,0);line=fgetl(fid);
        posE=findstr(line,'E'); %find E of subcase
        subcasestr=line(posE+1:end);
        subcasestrold;
        if strcmp(subcasestr,subcasestrold)~=1
            caseopt=caseopt+1; %new subcase
            filecounter=0;
        end
        subcasestrold=subcasestr;
        fseek(fid,currentFPIhistory(2),-1);line=fgetl(fid);
    end
    
    if isempty(str1)==0 && isempty(str2)==0 && isempty(str3)==0
        line=fgetl(fid);currentFPI=ftell(fid);
        line=fgetl(fid);currentFPI=ftell(fid);
        eqpos=find(line=='=');mpos=find(line=='M');
        point=str2num(line(eqpos(1)+1:mpos(1)-1));
        if currentpoint~=point
            flag=0;
            filecounter=filecounter+1;
        else
            flag=1;
        end
        currentpoint=point;
        line=fgetl(fid);currentFPI=ftell(fid);
        line=fgetl(fid);currentFPI=ftell(fid);
        line=fgetl(fid);currentFPI=ftell(fid);
        line=fgetl(fid);currentFPI=ftell(fid);
        if flag~=1
            ind=0;
        end
        while line(1)~='1' & line(2)~='*'
            ind=ind+1;
            eval(['data' num2str(filecounter) '(ind,:,caseopt-1)=[' line '];']);
            line=fgetl(fid);currentFPI=ftell(fid);
            if length(line)==0
%                 break
            end
        end
    end
end
fclose(fid);close(hbar);save lastpath pathname
if fatalerror==1
    disp('A Fatal error has been encountered. Code terminated.')
%     break
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %   SAVING DAMPING AND VELOCITY DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filecounter=filecounter+1;
if method==2%%PKNL-method
    for ijk=1:1:filecounter-1
        eval(['denratio(:,ijk,:)=data' num2str(ijk) '(:,3,:);'])
        eval(['vel(:,ijk,:)=data' num2str(ijk) '(:,3,:);'])
        eval(['damp(:,ijk,:)=data' num2str(ijk) '(:,4,:);'])
        eval(['freq(:,ijk,:)=data' num2str(ijk) '(:,5,:);'])
        eval(['REAL(:,ijk,:)=data' num2str(ijk) '(:,6,:);'])
        eval(['IMAG(:,ijk,:)=data' num2str(ijk) '(:,7,:);'])
    end
    EAS=sqrt(denratio).*vel;%%EAS-Equivalent Air Speed
    vel=EAS;
elseif method==1 || method ==3 || method==4%%K METHOD, KE METHOD AND PK METHOD
    for ijk=1:1:filecounter-1
        eval(['vel(:,ijk,:)=data'  num2str(ijk) '(:,3,:);'])
        eval(['damp(:,ijk,:)=data' num2str(ijk) '(:,4,:);'])
        eval(['freq(:,ijk,:)=data' num2str(ijk) '(:,5,:);'])
        eval(['REAL(:,ijk,:)=data' num2str(ijk) '(:,6,:);'])
        eval(['IMAG(:,ijk,:)=data' num2str(ijk) '(:,7,:);'])
    end
end
%% Convert Unit of Speed to other Units
if ConvertUnit==1%%ft/s
    vel=vel/12;
elseif ConvertUnit==2%%knots
    vel=vel*0.0493736501;
elseif ConvertUnit==0%%inch/s
    vel=vel;
elseif ConvertUnit==3
    vel=vel*convert_vel_value;
end
%%
damp=damp-CriticalDampingValue;
%loop to check if velocity vectors are all the same
flagvel=0;
for ijk=1:1:filecounter-2
    if vel(:,ijk)==vel(:,ijk+1)
        flagvel=flagvel+1;
    end
end

cc=1;Flutter=[];

%% Create filename
% save flutter results
fid102=fopen([results_folder filesep 'flutter_results_summary.txt'],'w+');
fclose(fid102);
fid102=fopen([results_folder filesep 'flutter_results_summary.txt'],'a');

string='Solution not converged for certain speeds.';
for ijk=1:1:filecounter-1
    posflutter=find(damp(:,ijk)>=0);
    divergencetest=find(freq(:,ijk)<=eps);
    for ppp=1:1:length(posflutter)
        if posflutter(ppp)==1
            
        elseif damp(posflutter(ppp)-1,ijk)*damp(posflutter(ppp),ijk)<0
            trueflutter=posflutter(ppp);
            X=[damp(trueflutter-1,ijk) damp(trueflutter,ijk)];
            Y=[vel(trueflutter-1,ijk) vel(trueflutter,ijk)];
            Flutter(cc)=interp1(X,Y,0,'linear');
            X=[vel(trueflutter-1,ijk) vel(trueflutter,ijk)];
            Y=[freq(trueflutter-1,ijk) freq(trueflutter,ijk)];
            flutfreq(cc)=interp1(X,Y,Flutter(cc),'linear');
            mode(cc)=ijk;
            if cc==1
                disp(['Flutter results for file "' filename '"'])
            end
            if isempty(find(Y==0))==0
                if isempty(divergencetest)==0
                    flutter_summary = ['Possible flutter. Mode ' num2str(mode(cc)) ' at ' num2str(Flutter(cc)) ' has zero frequency. DIVERGENCE. ' string];
                    disp(flutter_summary)
                   fprintf(fid102,'%s\n',flutter_summary);
                else
                    flutter_summary = ['Possible flutter. Mode ' num2str(mode(cc)) ' at ' num2str(Flutter(cc)) ' has zero frequency. DIVERGENCE.'];
                     disp(flutter_summary)
                   fprintf(fid102,'%s\n',flutter_summary);
                    
                end
            else
                if isempty(divergencetest)==0
                    flutter_summary = ['Possible flutter. Mode ' num2str(mode(cc)) ' at ' num2str(Flutter(cc)) ' at ' num2str(flutfreq(cc)) ' Hz. ' string];
                      disp(flutter_summary)
                   fprintf(fid102,'%s\n',flutter_summary);
                 
                else
                    flutter_summary = ['Possible flutter. Mode ' num2str(mode(cc)) ' at ' num2str(Flutter(cc)) ' at ' num2str(flutfreq(cc)) ' Hz.'];
                     disp(flutter_summary)
                   fprintf(fid102,'%s\n',flutter_summary);
                end
            end
            cc=cc+1;
%             break
        end
       
    end
   
end
fclose(fid102);
%Flutter=[];
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                                PLOTTING V-G AND V-F PLOTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

color='krbgcmkrbgc';
symbol='ov>sdv<>oph';
style='-';
pqr=1;iii=1;

legendstr=['hl=legend('''];
for ijk=1:1:filecounter-1
    if ijk==filecounter-1
        legendstr=[legendstr 'Mode ' num2str(ijk) ''');'];
    else
        legendstr=[legendstr 'Mode ' num2str(ijk) ''','''];
    end
end
% legendstr=[legendstr 'Location','NorthWest'];
    


%% 
freq_1_elastic_mode = freq(:,7);

zeros_term = find(freq_1_elastic_mode == 0);

minimal_term_id = min(zeros_term);

removed_terms = length(freq_1_elastic_mode) - minimal_term_id + 1;

if isempty(removed_terms)
    
   removed_terms= 0;
end

velocities_num = size(vel,1) -removed_terms  ;


% remove zero frequency in short period motion - just for BFF case

% plot full range velocities
velocities_num_half = velocities_num ;%- length(find(data6(:,1)==0));%round(velocities_num/2);

%% Plot Root locus
modeNo=input('How many modes in the V-g & V-f plot?: ');

hf=figure(300);set(hf,'name',['root locus for file ' filename]);

axes1 = axes('Parent',hf,'YMinorTick','on','XMinorTick','on',...
    'LineWidth',1,...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');





for ijk=1:1:modeNo
    ht=hf;
    %eval(['plot(data' num2str(ijk) '(:,3,:),data' num2str(ijk) '(:,5,:),''' [style symbol(iii) color(pqr)] ''')'])
    if ijk==6
        plot(REAL(1:velocities_num_half ,ijk),IMAG(1:velocities_num_half ,ijk),[style symbol(iii) color(iii)],...
            'LineWidth',1,...
            'MarkerSize',10,...
            'MarkerEdgeColor',color(iii),...
            'MarkerFaceColor',color(pqr));%%change color(pqr)to color(iii)
    else
        plot(REAL(1:velocities_num_half ,ijk),IMAG(1:velocities_num_half ,ijk),[style symbol(iii) color(iii)],...
            'LineWidth',1,...
            'MarkerSize',10,...
            'MarkerEdgeColor',color(iii),...
            'MarkerFaceColor',color(pqr));%%change color(pqr)to color(iii)
    end
    hxt=xlabel('Real part, 1/sec','FontSize',15);
    
    hyt=ylabel('Imag part, rad/sec','FontSize',15);
    htt=title('Root-Locus','FontSize',15);grid off;hold on
    if iii==length(symbol)
        iii=1;pqr=pqr+1;
    else
        iii=iii+1;
    end
end
% eval(legendstr);
grid on;hold off;
figure(300);hold on;
AXIS=axis;
plot([0,0],[AXIS(3),AXIS(4)],'k-','LineWidth',1);hold off;

% Create legend
% legend1 = legend(axes1,'show');
% set(legend1,'Location','NorthWest');
%
figurename=['RootLocus_' modelname];

cd(results_folder);
saveas(gca,figurename,'jpeg');saveas(gca,figurename,'fig');
cd(program_folder);

%% Plot VF curve
hf100=figure(100);set(hf100,'name',['vf-plot for file ' filename]);
axes1 = axes('Parent',hf100,'YMinorTick','on','XMinorTick','on',...
    'LineWidth',1,...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

iii=1;pqr=1;
for ijk=1:1:modeNo
    ht=hf100;
    %eval(['plot(data' num2str(ijk) '(:,3,:),data' num2str(ijk) '(:,5,:),''' [style symbol(iii) color(pqr)] ''')'])
    plot(vel(1:velocities_num_half ,ijk),freq(1:velocities_num_half ,ijk),[style symbol(iii) color(iii)],...
        'LineWidth',1,...
        'MarkerSize',10,...
        'MarkerEdgeColor',color(iii),...
        'MarkerFaceColor',color(pqr));%%change color(pqr)to color(iii)
    if ConvertUnit==0
        hxt=xlabel('Air speed (knots)','FontSize',15);
    elseif ConvertUnit==1
        hxt=xlabel('Equiv.Air Speed (ft/s)','FontSize',15);
    elseif ConvertUnit==2
        hxt=xlabel('Equiv.Air Speed (Knots)','FontSize',15);
    end
    hyt=ylabel('Frequency (Hz)','FontSize',15);
    htt=title('V-f plot','FontSize',15);grid off;hold on
    if iii==length(symbol)
        iii=1;pqr=pqr+1;
    else
        iii=iii+1;
    end
end
eval(legendstr);
% set(legendstr,'Location','NorthEastOutside');
nn=axis;
Ylim=nn(3:4);
%

%% figure(100)
figure (100),hold on;
% legendamp=['legend=('damping=' num2str(CriticalDampingValue))']
if isempty(Flutter)==0
    for ijk=1:1:length(Flutter)
        eval(['hline' num2str(ijk) '=plot([Flutter(ijk) Flutter(ijk)],Ylim,''k'');']);
    end
end
hold off
legend1 = legend(axes1,'show');
set(legend1,'Location','NorthEastOutside');

figurename=['VFplot_' modelname];
cd(results_folder);
saveas(gca,figurename,'jpeg');
saveas(gca,figurename,'fig');
cd(program_folder);
%% Plot VG curve
hvf=figure(200);set(hvf,'name',['vg-plot for file ' filename]);
axes1 = axes('Parent',hvf,'YMinorTick','on','XMinorTick','on',...
    'LineWidth',1,...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

iii=1;pqr=1;
% for ijk=1:1:filecounter-1
for ijk=1:1:modeNo
    hb=hvf;
    %eval(['plot(data' num2str(ijk) '(:,3,:),damp(:,' num2str(ijk) ',:),''' [style symbol(iii) color(pqr)] ''')'])
    plot(vel(1:velocities_num_half ,ijk),damp(1:velocities_num_half ,ijk)+CriticalDampingValue,[style symbol(iii) color(iii)],...
        'LineWidth',1,...
        'MarkerSize',10,...
        'MarkerEdgeColor',color(iii),...
        'MarkerFaceColor',color(pqr))%%damp doesn't change, so add the CritialDampingValue, and change color(pqr)to color(iii)
    %     hxb=xlabel('Equiv.Air Speed (inch/s)','FontSize',15);
    if ConvertUnit==0
        hxb=xlabel('Air Speed (knots)','FontSize',15);
    elseif ConvertUnit==1
        hxb=xlabel('Air Speed (ft/s)','FontSize',15);
    elseif ConvertUnit==2
        hxb=xlabel('Air Speed (Knots)','FontSize',15);
    end
    hyb=ylabel('Damping','FontSize',15);
    htb=title('V-g plot','FontSize',15);grid off;hold on
    if iii==length(symbol)
        iii=1;pqr=pqr+1;
    else
        iii=iii+1;
    end
end

% figure (200),hold on,plot([axis(1),axis(2)],[CriticalDampingValue,CriticalDampingValue],'k-','linewidth',2)
mm=axis;
Ylim=[-0.5 0.5];%%damping upper and bottom bound in vf plot
figure (200),hold on,plot([mm(1),mm(2)],[CriticalDampingValue,CriticalDampingValue],'k-','linewidth',2)
% legendamp=['legend=('damping=' num2str(CriticalDampingValue))']
if isempty(Flutter)==0
    for ijk=1:1:length(Flutter)
        eval(['hline' num2str(ijk) '=plot([Flutter(ijk) Flutter(ijk)],Ylim,''k'');']);
    end
end
%%
kids=get(gcf,'children');set(kids(1),'Ylim',Ylim);
eval(legendstr);
posF=get(hf,'position');ly0old=posF(4);
posT=get(ht,'position');
posB=get(hb,'position');
posL=get(hl,'position');
if posL(4)>1
    lengthfactor=(posL(4)-1+posB(2))*posF(4);
    yy0=posF(2)-lengthfactor;
    ly0=posF(4)+lengthfactor;
else
    lengthfactor=0;
    yy0=posF(2)-lengthfactor;
    ly0=posF(4)+lengthfactor;
end
lf=(posL(3)+2-posB(1)-posB(3))*posF(3);
% set(hf,'position',[posF(1) yy0 lf ly0]);
% set(ht,'position',[posT(1:2) posT(3)*posF(3)/lf posT(4)])
% set(hb,'position',[posB(1:2) posB(3)*posF(3)/lf posB(4)])
%set(hl,'position',[posF(3)/lf posT(2)+posT(4)-posL(4) posL(3:4)])
set(hl,'position',[posF(3)/lf posT(2)+posT(4)-posL(4)*ly0old/ly0 posL(3) posL(4)*ly0old/ly0])
figurename=['VGplot_' modelname];
cd(results_folder);
saveas(gca,figurename,'jpeg');
saveas(gca,figurename,'fig');
cd(program_folder);
%
% dampaxis=[get(kids(1),'Xlim') get(kids(1),'Ylim')];
% freqaxis=[get(kids(2),'Xlim') get(kids(2),'Ylim')];
% Fstring=num2str(freqaxis);lineIN=double(Fstring);lineOUT=elimination(lineIN);Fstring=char(lineOUT);
% Dstring=num2str(dampaxis);lineIN=double(Dstring);lineOUT=elimination(lineIN);Dstring=char(lineOUT);
%
% sizetext=35;edittext=30;framespace=2;hightexe=50;
% timeschanges=0;timeschanges0=0;



